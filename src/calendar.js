import { Calendar, dateFnsLocalizer } from "react-big-calendar";
import format from "date-fns/format";
import parse from "date-fns/parse";
import startOfWeek from "date-fns/startOfWeek";
import getDay from "date-fns/getDay";
import "react-big-calendar/lib/css/react-big-calendar.css";
import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const locales = { "pt-Br": require("date-fns/locale/pt-BR") };

const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

// Formato que os dados vão vir do banco:

const lista_entidades_consulta = [
  {
    id_consulta: 1,
    id_paciente: 1,
    id_medico: 3,
    dt_inicio: new Date(2021, 12, 12, 9),
    dt_fim: new Date(2021, 12, 12, 10),
    ds_status: "pendente",
  },
  {
    id_consulta: 2,
    id_paciente: 2,
    id_medico: 2,
    dt_inicio: new Date(2021, 12, 12, 15),
    dt_fim: new Date(2021, 12, 12, 16),
    ds_status: "pendente",
  },
  {
    id_consulta: 3,
    id_paciente: 4,
    id_medico: 5,
    dt_inicio: new Date(2021, 12, 11, 9),
    dt_fim: new Date(2021, 12, 11, 10),
    ds_status: "pendente",
  },
];

// Tranformado para ser lido pela lib de Calendar

const eventsList = [
  {
    title: "Big Vacation",
    start: new Date(2021, 12, 0),
    end: new Date(2021, 12, 0),
  },
  {
    title: "Consulta 3",
    start: new Date(2021, 12, 0),
    end: new Date(2021, 12, 0),
  },
];

const transform = (obj) => {
  return {
    title: obj.id_paciente,
    start: obj.dt_inicio,
    end: obj.dt_fim,
  };
};

const MyCalendar = (props) => {
  const [newEvent, setNewEvent] = useState({ title: "", start: "", end: "" });
  const [allEvents, setAllEvents] = useState(eventsList);

  const HandleAddEvent = () => {
    setAllEvents([...allEvents, newEvent]);
  };

  return (
    <div className="app">
      <h1>Agenda de consultas</h1>
      <h2>Marcar nova consulta</h2>
      <div
        style={{
          display: "flex",
          flexFlow: "column wrap",
          width: "500px",
          border: "1px solid black",
          margin: "0 auto",
        }}
      >
        <input
          type="text"
          placeholder="Add title"
          style={{ width: "200px", margin: "0 auto" }}
          value={newEvent.title}
          onChange={(e) => setNewEvent({ ...newEvent, title: e.target.value })}
        ></input>
        <DatePicker
          placeholderText="Start Date"
          selected={newEvent.start}
          onChange={(start) => setNewEvent({ ...newEvent, start })}
        />
        <DatePicker
          placeholderText="End Date"
          selected={newEvent.end}
          onChange={(end) => setNewEvent({ ...newEvent, end })}
        />
        <button
          onClick={HandleAddEvent}
          style={{ width: "200px", margin: "0 auto" }}
        >
          Confirmar
        </button>
      </div>
      <Calendar
        localizer={localizer}
        events={allEvents}
        startAccessor="start"
        endAccessor="end"
        style={{ height: 500, margin: "50px" }}
      />
    </div>
  );
};

export default MyCalendar;
